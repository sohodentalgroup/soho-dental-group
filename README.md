We are dedicated to maintaining the highest standard of care in every service we provide and look forward to helping you and your family preserve your beautiful smiles for life.

Address: 552 Broadway, Suite 505, New York, NY 10012, USA

Phone: 212-878-7646

Website: https://www.sohodentalgroup.com/